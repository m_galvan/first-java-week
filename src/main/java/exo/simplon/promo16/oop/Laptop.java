package exo.simplon.promo16.oop;

public class Laptop {
    private String model;
    private int battery;
    private boolean turnedOn;
    private boolean pluggedIn;

    public Laptop(String model) {
        this.model = model;
        this.battery = 0;
        this.turnedOn = false;
        this.pluggedIn = false;
    }

    @Override
    public String toString() {
        return model + " " + battery + " " + turnedOn + " " + pluggedIn;
    }

    public void plug() {
        pluggedIn = true;
        battery += 10;
    }

    public void powerSwitch() {
        if (turnedOn) {
            turnedOn = false;
        } else if (!turnedOn && pluggedIn) {
            turnedOn = true;
            System.out.println(turnedOn);
        } else if (!turnedOn && !pluggedIn && battery > 10) {
            battery -= 5;
            System.out.println(battery);
        } else {
            System.out.println("Je fais rien !");
        }

    }
}
