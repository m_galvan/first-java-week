package exo.simplon.promo16;

import exo.simplon.promo16.bases.Conditions;
import exo.simplon.promo16.bases.Variables;
import exo.simplon.promo16.oop.Laptop;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {

        // VARIABLES
        Variables variables = new Variables();
        // variables.first();
        // variables.withParameters("Bloup ", 7);
        // System.out.println(variables.withReturn());

        // CONDITIONS
        Conditions conditions = new Conditions();
        // System.out.println(conditions.isPositive(3));
        // System.out.println(conditions.skynetIA("How are you ?"));
        // conditions.buy(3, "Alcohol");
        // System.out.println(conditions.greater(4, 2));

        // LAPTOP
        Laptop laptop = new Laptop("iPhone");
        // System.out.println(laptop);
        // laptop.plug();
        laptop.powerSwitch();
    }
}